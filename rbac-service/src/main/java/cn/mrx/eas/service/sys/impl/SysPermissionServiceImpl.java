package cn.mrx.eas.service.sys.impl;

import cn.mrx.eas.dao.sys.SysPermissionMapper;
import cn.mrx.eas.dto.sys.SysPermission;
import cn.mrx.eas.exception.EasException;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;
import cn.mrx.eas.service.sys.ISysPermissionService;
import cn.mrx.eas.vo.sys.TreeNode;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/24 下午2:52
 * Description:
 */
@Service
public class SysPermissionServiceImpl implements ISysPermissionService {

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public BSGrid permissionListPage(Integer curPage, Integer pageSize, String name) {
        Page<SysPermission> sysPermissionPage = PageHelper.startPage(curPage, pageSize);
        List<SysPermission> sysPermissionList = sysPermissionMapper.permissionListPage(name);
        return new BSGrid(true, curPage, pageSize, sysPermissionPage.getTotal(), null, null, sysPermissionList);
    }

    @Override
    public SysPermission findOneById(Integer id) {
        SysPermission sysPermission = sysPermissionMapper.findOneById(id);
        if (sysPermission == null) {
            throw new EasException(404, "没有该条数据");
        }
        return sysPermission;
    }

    @Override
    public List<TreeNode> allNodes() {
        List<SysPermission> sysPermissionList = sysPermissionMapper.findAll();
        List<TreeNode> treeNodeList = new ArrayList<>();
        for (SysPermission sysPermission : sysPermissionList) {
            TreeNode treeNode = new TreeNode();
            treeNode.setId(sysPermission.getId());
            treeNode.setParentId(sysPermission.getParentId());
            treeNode.setName(sysPermission.getName());
            treeNodeList.add(treeNode);
        }
        return treeNodeList;
    }

    @Transactional
    @Override
    public ServerResponse editPermission(SysPermission sysPermission) {
        int result = sysPermissionMapper.editPermission(sysPermission);
        if (result > 0) return ServerResponse.success("修改成功");
        return ServerResponse.error("修改失败");
    }

    @Transactional
    @Override
    public ServerResponse addPermission(SysPermission sysPermission) {
        int result = sysPermissionMapper.addPermission(sysPermission);
        if (result > 0) return ServerResponse.success("添加成功");
        return ServerResponse.error("添加失败");
    }

    @Transactional
    @Override
    public ServerResponse batchDel(String ids) {
        int result = sysPermissionMapper.batchDel(ids.split(","));
        if (result > 0) return ServerResponse.success("批量操作成功");
        return ServerResponse.error("批量操作失败");
    }

    @Override
    public List<SysPermission> findAll() {
        return sysPermissionMapper.findAll();
    }

    @Override
    public List<SysPermission> findAllByUserId(Integer userId) {
        return sysPermissionMapper.findAllByUserId(userId);
    }
}