package cn.mrx.eas.dao.sys;


import cn.mrx.eas.dto.sys.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserMapper {

    List<SysUser> userListPage();

    SysUser findByUsername(String username);

    SysUser findOneById(Integer id);

    int editUser(SysUser sysUser);

    int addUser(SysUser sysUser);

    int batchDel(String[] split);
}